@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">
                        <div ><img src="/uploads/profile_pictures/{{$user->profile_picture}}" style="width: 150px; height: 150px; float: left; border-radius: 50%; margin-right: 25px;"></div>
                        <div>
                            <h2>{{$user->name}}'s Profile</h2>
                        </div>
                        <form  enctype="multipart/form-data" action="{{ url('#') }}" method="post">
                            <label>Update Profile Image</label>
                            <input type="file" name="profile_picture">
                            <input type="hidden" name="token" value="{{ csrf_token() }}">
                            <input type="submit" class="btn btn-sm btn-primary" value="Add">
                            {{ csrf_field() }}
                        </form>
                    </div>
                    <div class="card-body">

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
