@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Employee Information</div>

                    <div class="card-body">
                        <div>ID: {{$users->id}}</div>
                        <div>Name: {{$users->name}}</div>
                        <div>Email: {{$users->email}}</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
