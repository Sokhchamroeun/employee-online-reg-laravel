@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Employee List</div>

                <div class="card-body">
                    <table class="table table-striped">
                        <thead >
                        <tr>
                            <th scope="col">ID </th>
                            <th scope="col">Name</th>
                            <th scope="col">Email</th>
                            <th scope="col">Profile Picture</th>
                            <th scope="col">Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if(count($users) > 0)
                            @foreach($users->all() as $user)
                        <tr>
                            <th scope="row">{{$user->id}}</th>
                            <td>{{$user->name}}</td>
                            <td>{{$user->email}}</td>
                            <td>profile_picture</td>
                            <td>
                                <a href='{{ url("/view_employee/{$user->id}") }}' class="badge badge-info">View</a>
                                <a href='{{ url("/edit_employee/{$user->id}")}}' class ="badge badge-success">Edit</a>

                                <a href='{{ url("/delete_employee/{$user->id}")}}' class="badge badge-danger">Delete</a>
                            </td>
                        </tr>
                            @endforeach
                        @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
