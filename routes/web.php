<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function(){
    return view('welcome');
});
Route::get('/index', 'UserController@index');
Route::get('/employee_list', 'UserController@index');
Route::get('/view_employee/{id}', 'UserController@show');
Route::get('/edit_employee/{id}', 'UserController@edit');
Route::post('/update_employee/{id}', 'UserController@update');
Route::get('/delete_employee/{id}', 'UserController@destroy');
Route::get('/profile', 'UserController@profile');

Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');
